import axios from 'axios';

export function get(url, params = null) {
	return axios.get(url, params);
}

export function post(url, params = null, config = null) {
	return axios.post(url, params, config);
}

export function put(url, params = null, config = null) {
	return axios.put(url, params, config);
}

export function remove(url, params = null) {
	return axios.delete(url, params);
}
