import React, { useEffect, useState } from 'react';

const InstallPWA = () => {
	const [supportsPWA, setSupportsPWA] = useState(false);
	const [promptInstall, setPromptInstall] = useState(null);

	useEffect(() => {
		const handler = (e) => {
			e.preventDefault();
			setSupportsPWA(true);
			setPromptInstall(e);
		};
		window.addEventListener('beforeinstallprompt', handler);

		return () => window.removeEventListener('transitionend', handler);
	}, []);

	const onClick = (evt) => {
		evt.preventDefault();
		if (!promptInstall) {
			return;
		}
		promptInstall.prompt();
	};
	if (!supportsPWA) {
		return null;
	}
	console.log(supportsPWA);
	return (
		<div className="relative ">
			<div className="absolute inset-x-0 top-0 ml-3 flex flex-col justify-center bg-white border border-black shadow-md w-11/12 mb-5 z-50 rounded-md px-1">
				<button
					className="link-button bg-green-600 mt-2 rounded-md px-10 text-xl text-white font-semibold"
					id="setup_button"
					aria-label="Install app"
					title="Install app"
					onClick={onClick}
				>
					Instalar
				</button>
				<p className="py-2 text-xs px-5 font-semibold text-center m-0">
					Click aquí para instalar tu app
				</p>
			</div>
		</div>
	);
};

export default InstallPWA;
