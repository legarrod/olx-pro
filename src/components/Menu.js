import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import './style.menu.css';

const MenuNav = () => {
	const showSettings = (event) => {
		event.preventDefault();
	};

	return (
		<Menu className="menuPrincipal h-full">
			<a id="home" className="menu-item" href="/">
				Inicio
			</a>
			<a id="about" className="menu-item" href="/login">
				Login
			</a>
			<a id="contact" className="menu-item" href="/registraradministrador">
				Registro de administrador
			</a>
			<a id="contact" className="menu-item" href="/registroUsuario">
				Registro de usuario
			</a>
			<a id="contact" className="menu-item" href="/datosanuncio">
				Crear anuncio
			</a>
			<a id="contact" className="menu-item" href="/perfil">
				Perfil
			</a>
			<a
				onClick={(e) => showSettings(e)}
				href="/administrador"
				className="menu-item--small"
			>
				administrador
			</a>
		</Menu>
	);
};

export default MenuNav;
