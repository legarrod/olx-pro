import React from 'react';

const Footer = () => {
	return (
		<div
			className="grid pt-3 md:grid-cols-2 bg-blue-900 "
			style={{ position: 'relative' }}
		>
			<p className="px-10 pb-2 m-0 pt-2 text-white font-semibold items-center">
				© 2020 | Proyecto formativo SENA
			</p>

			<p className="px-10 pb-2 m-0 pt-2 text-xs md:text-sm text-white font-semibold">
				Realizado por: James Oyola | Smith Angel | Sebastian Chica | Luis García
			</p>
		</div>
	);
};

export default Footer;
