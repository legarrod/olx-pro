import React, { useCallback, useEffect, useState } from 'react';
import './style.categorias.css';
import axios from 'axios';
import { Categoria } from '../../bd';
import { useTranslation } from 'react-i18next';

const Categorias = ({ categorias }) => {
	const [categoriasInfo, setCategoriasInfo] = useState(Categoria);
	// useEffect(() => {
	// 	getBusiness();
	// });
	// const getBusiness = useCallback(async () => {
	// 	try {
	// 		const { data } = await axios(
	// 			// `${process.env.REACT_APP_API_ORDERS}/business/${business_id}`
	// 			`${Categoria}`
	// 		);
	// 		console.log(data);
	// 		setCategoriasInfo(data);
	// 	} catch (error) {
	// 		console.log(error.message);
	// 	}
	// }, []);
	const { t, i18n } = useTranslation();
	return (
		<div className="">
			<div className="text-center md:text-left">
				<p className="text-3xl font-bold pl-2 pt-2">{t('menu.categories')}</p>
			</div>
			<div className="flex flex-wrap">
				{categoriasInfo.map((item) => (
					<div className="grid mr-4 mt-5 grid-cols-1 justify-center h-32 w-32">
						<div className="grid grid-cols-1 self-center items-center rounded-full h-32 w-32 bg-blue-600">
							<center>
								<img src={item.icono} alt="CES" className="h-20 w-20" />
							</center>
						</div>
						<div className="text-center">
							<p className="font-semibold ml-2 text-xl  text-blue-800">
								{item.nombre}
							</p>
						</div>
					</div>
				))}
			</div>
		</div>
	);
};

export default Categorias;
