import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Login from '../components/auth/Login';
import Registro from '../components/auth/Registro';
import Home from '../components/pages/Home';
import Detalle from '../components/Anuncios/DetalleAnuncio';
import MenuNav from '../components/Menu';
import DatosAnuncio from './Configuraciones/DatosAnuncio';
import EditarAnuncios from './Anuncios/EditarAnuncios';
import Perfil from './Configuraciones/PerfilUsuario';
import Superusuario from './Configuraciones/Superusuario';
import RegistroUsuario from './auth/RegistroUsuario';
import AuthState from '../context/AuthState';
import { RequiredAuth, RequiredAuthSuper } from '../context/RequiredAuth';

const Routes = () => {
	return (
		<>
			<AuthState>
				<Router>
					<Switch>
						<Route exact path="/" component={Home} />
						<Route exact path="/login" component={Login} />
						<Route exact path="/registraradministrador" component={Registro} />
						<Route exact path="/detalleAnuncio/:id" component={Detalle} />
						<Route
							exact
							path="/DatosAnuncio"
							component={(props) => (
								<RequiredAuth {...props} Component={DatosAnuncio} />
							)}
						/>
						<Route
							exact
							path="/perfil"
							component={(props) => (
								<RequiredAuth {...props} Component={Perfil} />
							)}
						/>
						<Route exact path="/registroUsuario" component={RegistroUsuario} />
						<Route
							exact
							path="/superUsuario"
							component={(props) => (
								<RequiredAuthSuper {...props} Component={Superusuario} />
							)}
						/>
						<Route
							exact
							path="/EditarAnuncios/:id_anuncio"
							component={(props) => (
								<RequiredAuthSuper {...props} Component={EditarAnuncios} />
							)}
						/>
						<Route exact path="/detalleAnuncio/:id" component={Detalle} />
						<Route exact path="/detalleAnuncio/:id" component={Detalle} />
						<Route exact path="/detalleAnuncio/:id" component={Detalle} />
						<Route exact path="/detalleAnuncio/:id" component={Detalle} />
					</Switch>
				</Router>
			</AuthState>
		</>
	);
};

export default Routes;
