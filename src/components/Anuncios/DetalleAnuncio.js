import React, { useState, useEffect } from 'react';
import ReactMediumImg from 'react-medium-zoom';
import Header from '../Header/Header';
import MenuCategorias from '../MenuCategorias/MenuCategorias';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import './style.anuncios.css';
import Paper from '@material-ui/core/Paper';
import AnuncioDestacado from '../Anuncios/AnuncioDestacado';
import { get } from '../../libs/AsyncHttpRequest';
import { formatNumber } from '../../utils/FormatNumberCop';

const useStyles = makeStyles((theme) => ({
	card: {
		overflow: 'visible',
	},
	imageHeader: {
		position: 'relative',
		[theme.breakpoints.down('sm')]: {
			borderRadius: `${theme.shape.borderRadius}px ${theme.shape.borderRadius}px 0 0`,
		},
		[theme.breakpoints.up('sm')]: {
			top: theme.spacing(1) / -2,
			left: theme.spacing(1) / -2,
			boxShadow: theme.shadows[5],
			borderRadius: theme.shape.borderRadius,
		},
		objectFit: 'cover',
		minHeight: '400px',
		height: '100%',
		width: '100%',
		backgroundSize: 'cover',
		backgroundRepeat: 'no-repeat',
		backgroundImage: `url(https://www.redcomingenieria.cl/images/no-imagen.jpg)`,
		backgroundPosition: 'top',
	},
	tabContainer: {
		flexDirection: 'column',
		backgroundColor: theme.palette.background.paper,
	},
	tabRoot: {
		textTransform: 'initial',
		color: theme.palette.text.primary,
	},
	tabsRoot: {
		borderBottom: `1px solid ${theme.palette.divider}`,
	},
	inactive: {
		color: theme.palette.text.secondary,
	},
	group: {
		display: 'inline-block',
		boxShadow: theme.shadows[2],
		borderRadius: theme.shape.borderRadius,
	},
	groupButton: {
		borderRadius: 0,
		boxShadow: 'none',
		minWidth: '40px',
		'&:first-child': {
			borderRadius: `${theme.shape.borderRadius}px 0 0 ${theme.shape.borderRadius}px`,
		},
		'&:last-child': {
			borderRadius: `0 ${theme.shape.borderRadius}px ${theme.shape.borderRadius}px 0`,
		},
	},
	optionTitle: {
		minWidth: '50px',
	},
}));

const Detalle = (props) => {
	const queryId = props.match.params.id;
	const [detalleAnuncio, setDetalleAnuncio] = useState({});
	const classes = useStyles();
	const [anuncios, setAnuncios] = useState([]);
	const [verContacto, setVerContacto] = useState(false);
	const [telefono, setTelefono] = useState('');

	useEffect(() => {
		const getAnuncios = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_ANUNCIO_USUARIO}`
				);
				const infoAnuncio = Object.assign(response.data);
				setAnuncios(infoAnuncio);
			} catch (error) {
				console.log(error);
			}
		};
		getAnuncios();
	}, [queryId]);

	useEffect(() => {
		setDetalleAnuncio(
			anuncios.find((anuncio) => anuncio.idAnuncio === parseInt(queryId))
		);
		setVerContacto(true);
	}, [queryId, anuncios]);
	const getTelefono = () => {
		setTelefono(detalleAnuncio.usuario.telefono);
	};

	return (
		<>
			<Header></Header>
			<MenuCategorias />
			<div className="container mt-5 md:mx-5 h-64 items-center">
				{detalleAnuncio && (
					<Paper variant="outlined" square className="p-2 mt-2 md:p-5 md:mt-5">
						<div className="grid mt-10 grid-cols-1 md:grid-cols-2">
							<ReactMediumImg
								className={classes.imageHeader}
								src={detalleAnuncio.urlFoto}
								title={detalleAnuncio.titulo}
								onOpen={() => 'Image Open'}
								onClosed={() => 'Image closed'}
							/>
							<div xs={12} sm={6} md={7} className="pa-1 mx-10">
								<div focus={true} className="">
									<div>
										<Typography variant="h5">
											{detalleAnuncio.titulo}
										</Typography>
										<Typography variant="subtitle1"></Typography>

										<Typography
											variant="h6"
											className="mt-1 justify-end"
											gutterBottom
										>
											<span>{`${formatNumber(detalleAnuncio.precio)}`}</span>
										</Typography>

										<Typography gutterBottom>
											{detalleAnuncio.descripcion}
										</Typography>

										<div className="block my-1" />
										{verContacto && (
											<div className="flex flex-wrap">
												<a
													onClick={getTelefono}
													href={`https://api.whatsapp.com/send?phone=+57${telefono}&text=Quiero%20mas%20informacion%20sobre%20el%20${detalleAnuncio.titulo}`}
													target="_blanck"
												>
													<img
														className="h-12 m-2 "
														src="https://s3.amazonaws.com/recursos.proyecto/whatsapp.png"
														alt=""
													/>
												</a>

												<a onClick={getTelefono} href={`tel:+${telefono}`}>
													<img
														className="h-12 m-2 block md:hidden"
														src="https://s3.amazonaws.com/recursos.proyecto/llamar.png"
														alt=""
													/>
												</a>
											</div>
										)}
									</div>
									{/* <div>
									<textarea
										name="textarea"
										className="campo-mensaje my-3 mx-0"
										placeholder="Escriba aquí su mensaje"
									></textarea>
								</div> */}
								</div>
								{/* <div className=" mb-10">
								<Button variant="contained" color="primary">
									Contactcar cliente
								</Button>
							</div> */}
							</div>
						</div>
					</Paper>
				)}

				<p className="font-bold text-2xl mt-5 mb-0">Productos destacados</p>
				<AnuncioDestacado></AnuncioDestacado>
			</div>
		</>
	);
};

export default Detalle;
