import React, { useState, useEffect, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ImageUploading from 'react-images-uploading';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import { red } from '@material-ui/core/colors';
import ShareIcon from '@material-ui/icons/Share';
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';
import Paper from '@material-ui/core/Paper';
import { get, remove, put } from '../../libs/AsyncHttpRequest';
import Swal from 'sweetalert2';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { USER_INFO } from '../../types';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles((theme) => ({
	root: {
		maxWidth: 345,
	},
	media: {
		height: 0,
		paddingTop: '56.25%', // 16:9
	},
	expand: {
		transform: 'rotate(0deg)',
		marginLeft: 'auto',
		transition: theme.transitions.create('transform', {
			duration: theme.transitions.duration.shortest,
		}),
	},
	expandOpen: {
		transform: 'rotate(180deg)',
	},
	avatar: {
		backgroundColor: red[500],
	},
}));

const ActualizarAnuncio = ({ fotoPerfil }) => {
	const classes = useStyles();
	const [editarAnuncio, setEditarAnuncio] = useState({});
	const infoUsuario = JSON.parse(localStorage.getItem(USER_INFO));
	const dni = infoUsuario.dni;
	const telefono = infoUsuario.telefono;
	const [verBoton, setVerBoton] = useState();
	const [categoria, setCategoria] = useState('');
	const [visivleSubcategories, setVisivleSubcategories] = useState(false);
	const [verCategoria, setVerCategoria] = useState([]);
	const [verInformacion, setverInformacion] = useState([]);
	const [subCategoria, setSubCategoria] = useState('');
	const [image, setImage] = useState('');
	const [subCategorias, setSubCategorias] = useState([]);
	const [loading, setloading] = useState(false);
	const [verActualziarInformacion, setVerActualziarInformacion] = useState(
		false
	);
	const [anuncios, setAnuncios] = useState([]);
	const { handleSubmit, register, errors } = useForm();
	useEffect(() => {
		const getAnuncios = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_ANUNCIO_USUARIO}`
				);
				const infoAnuncio = Object.assign(response.data);
				const nuevoAnuncio = infoAnuncio.filter(
					(item) => item.usuario.dni === dni
				);
				setAnuncios(nuevoAnuncio);
			} catch (error) {
				console.log(error);
			}
		};
		getAnuncios();
	}, []);

	useEffect(() => {
		const getCategoria = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_ONLY_CATEGORIA}`
				);

				const infoUsuario = Object.assign(response.data);

				setVerCategoria(infoUsuario);
			} catch (error) {
				console.log(error);
			}
		};
		const getSubCategoria = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_ONLY_SUB_CATEGORIA}`
				);

				const infoUsuario = Object.assign(response.data);

				setverInformacion(infoUsuario);
			} catch (error) {
				console.log(error);
			}
		};
		getCategoria();
		getSubCategoria();
	}, []);
	useEffect(() => {
		const subCate = verInformacion.filter(
			(option) => option.categoria.idCategoria === categoria.idCategoria
		);
		setSubCategorias(subCate);
	}, [categoria, verInformacion]);
	const handlerEliminarAnuncio = async (id) => {
		try {
			const response = await remove(
				`${process.env.REACT_APP_API_ELIMINAR_ANUNCIO}/${id}`
			);
		} catch (error) {
			console.log(error);
		}
	};
	const uploadImage = async (e) => {
		const files = e.target.files;
		const data = new FormData();
		data.append('file', files[0]);
		data.append('upload_preset', 'geekyimages');
		setloading(true);

		const res = await fetch(
			'https://api.cloudinary.com/v1_1/dhqgxx0cr/image/upload',
			{
				method: 'POST',
				body: data,
			}
		);
		const file = await res.json();
		setImage(file.secure_url);
		setloading(false);
	};
	const onSubmit = async (values) => {
		const idAnuncio = parseInt(values.idAnuncio);
		try {
			const { data } = await put(
				`${process.env.REACT_APP_API_ACTUALIZAR_ANUNCIO}/${idAnuncio}`,
				{
					idAnuncio: idAnuncio,
					titulo: values.titulo,
					descripcion: values.descripcion,
					precio: values.precio,
					contacto: telefono,
					urlFoto: image,
					usuario: {
						dni: infoUsuario.dni,
					},
					subcategoria: {
						idSubCategoria: subCategoria.idSubCategoria
							? subCategoria.idSubCategoria
							: verBoton,
					},
				}
			);
			if (data) {
				Swal.fire(
					'Bien hecho!',
					'Se ha registrado un nuevo anuncio',
					'success'
				);
			} else {
				Swal.fire('Error!', 'Algo salio mal intenta nuevamente', 'error');
			}
			setTimeout(function () {
				window.location.reload(false);
			}, 3000);
		} catch (error) {
			Swal.fire(
				'Error!',
				'Algo sali mal intenta de nuevo y verifica toda la información',
				'error'
			);
		}
	};
	const handlerObtenerAnuncio = (id) => {
		const anuncioEditar = anuncios.find(
			(anuncio) => anuncio.idAnuncio === parseInt(id.idAnuncio)
		);
		setSubCategorias(id.subcategoria.idSubCategoria);
		setVerBoton(id.subcategoria.idSubCategoria);
		setImage(id.urlFoto);
		setEditarAnuncio(anuncioEditar);
		setVerActualziarInformacion(true);
	};

	const handleCategoria = (event) => {
		setCategoria(event.target.value);
		setVisivleSubcategories(true);
	};
	const handleSubCategoria = (e) => {
		setSubCategoria(e.target.value);
	};

	return (
		<>
			<div className="flex justify-end pr-5">
				<a href="/datosanuncio" target="__blanck">
					<AddCircleRoundedIcon
						className="text-red-700"
						style={{ fontSize: 80 }}
					></AddCircleRoundedIcon>
				</a>
			</div>
			<div className="grid xsm:grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 mx-2">
				{anuncios.map((item) => (
					//on esto vamos a validar que anuncios destacados vamos a mostrar
					<div
						className={`shadow-2xl mx-2 flex-1 my-5 border-solid`}
						key={item.idAnuncio}
					>
						<Link to={`/detalleAnuncio/${item.idAnuncio}`} anuncios={anuncios}>
							<CardHeader
								avatar={
									<Avatar
										aria-label="recipe"
										className={classes.avatar}
										src={fotoPerfil}
									></Avatar>
								}
								title={item.titulo}
							/>

							<CardMedia
								className={classes.media}
								image={item.urlFoto}
								title={item.titulo}
							/>
						</Link>
						<CardContent className="">
							<p className="font-bold">{item.titulo}</p>
							<p
								variant="body2"
								color="textSecondary"
								component="p"
								className="hidden md:block md:truncate"
							>
								{item.descripcion}
							</p>
						</CardContent>
						<CardActions disableSpacing className="flex justify-between">
							<IconButton aria-label="share">
								<ShareIcon />
							</IconButton>
							<p className="font-bold my-2 mr-3 text-pink-700">{`$ ${item.precio}`}</p>
						</CardActions>
						<center>
							<a href="#actualizarUsuario">
								<Button
									className="mb-2"
									variant="contained"
									color="primary"
									onClick={() => handlerObtenerAnuncio(item)}
								>
									Actualziar anuncio
								</Button>
							</a>
							<Button
								className="mb-2"
								variant="contained"
								color="primary"
								onClick={() =>
									Swal.fire({
										title: 'Quieres eliminar este artículo?',
										text:
											'Eliminar este artículo es sinónimo que no lo podrás recuperar',
										icon: 'warning',
										showCancelButton: true,
										confirmButtonColor: '#3085d6',
										cancelButtonColor: '#d33',
										confirmButtonText: 'Si eliminarlo!',
									}).then((result) => {
										if (result.value) {
											handlerEliminarAnuncio(item.idAnuncio);

											Swal.fire(
												'Eliminado!',
												'Su artículo se eliminó correctamente ',
												'success'
											);
											setTimeout(function () {
												window.location.reload(true);
											}, 2000);
										}
									})
								}
							>
								Eliminar anuncio
							</Button>
						</center>
					</div>
				))}
			</div>
			{verActualziarInformacion && (
				<Paper>
					<div className="m-10">
						<div>
							<p className="text-blue-700 text-2xl">
								¿Que cambios quieres realizar?
							</p>
							<p className="text-xs ">
								Recuerda confirmar la categoria y la subcategoria.
							</p>
						</div>
						<div className="flex flex-wrap">
							<form
								id="actualizarUsuario"
								onSubmit={handleSubmit(onSubmit)}
								className="grid grid-cols-1"
							>
								<TextField
									style={{ display: 'none' }}
									type="number"
									id="idAnuncio"
									name="idAnuncio"
									variant="outlined"
									defaultValue={editarAnuncio.idAnuncio}
									inputRef={register({
										required: '',
									})}
									error={errors.name ? true : false}
									label={errors.name ? 'error' : ''}
								/>
								{/** Titulo */}
								<TextField
									className="w-full py-2 px-2 my-2"
									type="text"
									id="titulo"
									name="titulo"
									variant="outlined"
									defaultValue={editarAnuncio.titulo}
									inputRef={register({
										required: 'Para registrar un anuncio debe crear un titulo',
									})}
									error={errors.name ? true : false}
									label={errors.name ? 'error' : 'Titulo anuncio *'}
								/>
								{/** Precio */}
								<TextField
									className="w-full py-2 px-2 my-2"
									type="text"
									id="precio"
									name="precio"
									variant="outlined"
									defaultValue={editarAnuncio.precio}
									inputRef={register({
										required: 'El precio es requerido',
									})}
									error={errors.name ? true : false}
									label={errors.name ? 'error' : 'Precio *'}
								/>
								{/** Descripcio */}
								<TextField
									className="w-full py-2 px-2 my-2"
									type="text"
									id="descripcion"
									name="descripcion"
									variant="outlined"
									defaultValue={editarAnuncio.descripcion}
									inputRef={register({
										required: 'El descripción es requerido',
									})}
									error={errors.name ? true : false}
									label={errors.name ? 'error' : 'Descripción *'}
									helperText={errors.name ? errors.name.message : ''}
								/>
								<FormControl
									variant="filled"
									className={` w-full pt-2 pb-0 mb-0 px-2 my-2`}
								>
									<InputLabel id="demo-simple-select-filled-label">
										Categorias *
									</InputLabel>

									<Select
										name="categorias"
										className="mb-0"
										labelId="demo-simple-select-filled-label"
										id="demo-simple-select-filled"
										label="Categoria"
										value={categoria}
										onChange={handleCategoria}
									>
										{verCategoria.map((categories) => (
											<MenuItem key={categories.idCategoria} value={categories}>
												{categories.nombre}
											</MenuItem>
										))}
									</Select>
								</FormControl>
								{visivleSubcategories && (
									<FormControl
										variant="filled"
										className={` w-full pt-2 pb-0 mb-0 px-2 my-2`}
									>
										<InputLabel id="demo-simple-select-filled-label">
											Subcategorias
										</InputLabel>

										<Select
											name="subcategorias"
											className="mb-0"
											labelId="demo-simple-select-filled-label"
											id="demo-simple-select-filled"
											label="Subcategoria"
											value={subCategoria}
											onChange={handleSubCategoria}
											// required
										>
											{subCategorias.map((categories) => (
												<MenuItem
													key={categories.idSubCategoria}
													value={categories}
												>
													{categories.descripcion}
												</MenuItem>
											))}
										</Select>
									</FormControl>
								)}
								<>
									<input type="file" name="file" onChange={uploadImage} />

									{loading ? (
										<p>Subiendo...</p>
									) : (
										<img
											src={image === '' ? editarAnuncio.urlFoto : image}
											style={{ width: '200px', height: 'auto' }}
											alt=""
										/>
									)}
								</>
								<div>
									<Button
										type="submit"
										className="mb-2 w-64 mt-2"
										variant="contained"
										color="primary"
										// onClick={() => handlerSetInormation}
									>
										Actualziar
									</Button>
								</div>
							</form>
						</div>
					</div>
				</Paper>
			)}
		</>
	);
};
export default ActualizarAnuncio;
