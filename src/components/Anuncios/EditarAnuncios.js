import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { get } from '../../libs/AsyncHttpRequest';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import ShareIcon from '@material-ui/icons/Share';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
	root: {
		maxWidth: 345,
	},
	media: {
		height: 0,
		paddingTop: '56.25%', // 16:9
	},
	expand: {
		transform: 'rotate(0deg)',
		marginLeft: 'auto',
		transition: theme.transitions.create('transform', {
			duration: theme.transitions.duration.shortest,
		}),
	},
	expandOpen: {
		transform: 'rotate(180deg)',
	},
	avatar: {
		backgroundColor: red[500],
	},
}));

export default function EditarAnuncios() {
	const classes = useStyles();
	const [anuncios, setAnuncios] = useState([]);

	useEffect(() => {
		const getAnuncios = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_ANUNCIO_USUARIO}`
				);
				const infoAnuncio = Object.assign(response.data);

				setAnuncios(infoAnuncio);
			} catch (error) {
				console.log(error);
			}
		};
		getAnuncios();
	}, []);

	return (
		<div className="grid xsm:grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 mx-2">
			{anuncios.map((item) => (
				<div
					className={`shadow-2xl mx-2 flex-1 my-2 border-solid`}
					key={item.id}
				>
					<Link to={`/detalleAnuncio/${item.idAnuncio}`} anuncios={anuncios}>
						<CardHeader
							avatar={
								<Avatar aria-label="recipe" className={classes.avatar}>
									R
								</Avatar>
							}
							title={item.titulo}
						/>

						<CardMedia
							className={classes.media}
							image={item.urlFoto}
							title={item.titulo}
						/>
					</Link>
					<CardContent className="">
						<p className="font-bold">{item.titulo}</p>
						<p
							variant="body2"
							color="textSecondary"
							component="p"
							className="hidden md:block md:truncate"
						>
							{item.descripcion}
						</p>
					</CardContent>
					<CardActions disableSpacing className="flex justify-between">
						<IconButton aria-label="share">
							<ShareIcon />
						</IconButton>
						<p className="font-bold my-2 mr-3 text-pink-700">{`$ ${item.precio}`}</p>
					</CardActions>
				</div>
			))}
		</div>
	);
}
