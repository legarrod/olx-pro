import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { get } from '../../libs/AsyncHttpRequest';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import { useTranslation } from 'react-i18next';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import ShareIcon from '@material-ui/icons/Share';
import { Link } from 'react-router-dom';
import { formatNumber } from '../../utils/FormatNumberCop';

const useStyles = makeStyles((theme) => ({
	root: {
		maxWidth: 345,
	},
	media: {
		height: 0,
		paddingTop: '56.25%', // 16:9
	},
	expand: {
		transform: 'rotate(0deg)',
		marginLeft: 'auto',
		transition: theme.transitions.create('transform', {
			duration: theme.transitions.duration.shortest,
		}),
	},
	expandOpen: {
		transform: 'rotate(180deg)',
	},
	avatar: {
		backgroundColor: red[500],
	},
}));

export default function AnuncioDestacado() {
	const classes = useStyles();
	const { t, i18n } = useTranslation();
	const [anuncios, setAnuncios] = useState([]);

	useEffect(() => {
		const getAnuncios = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_ANUNCIO_USUARIO}`
				);
				const infoAnuncio = Object.assign(response.data);

				setAnuncios(infoAnuncio.sort(() => Math.random() - 0.5));
			} catch (error) {
				console.log(error);
			}
		};
		getAnuncios();
	}, []);

	return (
		<div className="grid xsm:grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 mx-2">
			{anuncios.map(
				(item) =>
					item.pay_add && ( //on esto vamos a validar que anuncios destacados vamos a mostrar
						<div
							className={`shadow-2xl mx-2 flex-1 my-5 border-solid border-2 border-gray-400`}
							key={item.idAnuncio}
						>
							<Link
								to={`/detalleAnuncio/${item.idAnuncio}`}
								anuncios={anuncios}
							>
								<p className="bg-red-600 w-2/5 ml-2 mt-2 font-semibold text-xs text-black text-center p-2">
									{t('home.outstanding')}
								</p>
								<CardHeader
									avatar={
										<Avatar
											aria-label="recipe"
											className={classes.avatar}
											src={item.usuario.url_Foto_usuario}
										></Avatar>
									}
									title={item.titulo}
								/>

								<CardMedia
									className={classes.media}
									image={item.urlFoto}
									title={item.titulo}
								/>
							</Link>
							<CardContent className="">
								<p className="font-bold">{item.titulo}</p>
								<p
									variant="body2"
									color="textSecondary"
									component="p"
									className="hidden md:block md:truncate"
								>
									{item.descripcion}
								</p>
							</CardContent>
							<CardActions disableSpacing className="flex justify-between">
								<a
									href={`https://www.facebook.com/sharer/sharer.php?u=http://localhost:3000/detalleAnuncio/${item.idAnuncio}`}
									target="_blanck"
								>
									<IconButton aria-label="share">
										<ShareIcon />
									</IconButton>
								</a>
								<p className="font-bold my-2 mr-3 text-pink-700">{`${formatNumber(
									item.precio
								)}`}</p>
							</CardActions>
						</div>
					)
			)}
		</div>
	);
}
