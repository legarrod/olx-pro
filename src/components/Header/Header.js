import React, { useState, useEffect } from 'react';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import AppBar from '@material-ui/core/AppBar';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import SearchIcon from '@material-ui/icons/Search';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/core/styles';
import Logo from '../../assets/img/logo.png';
import Badge from '@material-ui/core/Badge';
import PersonIcon from '@material-ui/icons/Person';
import { USER_KEY } from '../../types';
import { USER_INFO } from '../../types';
import Avatar from '@material-ui/core/Avatar';
import { useTranslation } from 'react-i18next';
import GTranslateIcon from '@material-ui/icons/GTranslate';
import { get } from '../../libs/AsyncHttpRequest';
import CardContent from '@material-ui/core/CardContent';
import CircularProgress from '@material-ui/core/CircularProgress';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import { formatNumber } from '../../utils/FormatNumberCop';

const useStyles = makeStyles((theme) => ({
	appBar: {
		boxShadow: '0 1px 8px rgba(0,0,0,.3)',
		position: 'relative',
		zIndex: theme.zIndex.drawer + 100,
		[theme.breakpoints.down('sm')]: {
			position: 'fixed',
		},
	},
	toolBar: {
		paddingLeft: theme.spacing(1) / 2,
		paddingRight: theme.spacing(1) / 2,
	},
	branding: {
		display: 'flex',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		whiteSpace: 'nowrap',
		margin: 'auto 0',
		lineHeight: '50px',
		padding: `0 64px 0 0`,
	},
	logo: {
		margin: 'auto',
		[theme.breakpoints.down('sm')]: {
			maxWidth: '200px',
		},
	},
	searchWrapper: {
		flex: '1 1 0%',
		boxSizing: ' border-box',
	},
	searchForm: {
		background: 'white',
		position: 'relative',
		borderRadius: theme.shape.borderRadius,
		marginRight: theme.spacing(1) * 2,
		display: 'block',
		maxWidth: '800px',
	},
	searchInput: {
		fontSize: '1rem',
		padding: theme.spacing(1) * 1.9,
		[theme.breakpoints.down('xs')]: {
			padding: theme.spacing(1) * 1.2,
		},
		cursor: 'text',
		textIndent: '30px',
		border: 'none',
		background: 'transparent',
		width: '100%',
		outline: '0',
	},
	searchIcon: {
		position: 'absolute',
		top: '50%',
		left: '0',
		marginTop: '-24px',
		color: 'rgba(0,0,0,.87)',
	},
	root: {
		display: 'flex',
	},
	details: {
		display: 'flex',
		flexDirection: 'column',
	},
	content: {
		flex: '1 0 auto',
	},
	cover: {
		width: 151,
	},
}));

const Header = ({ logoAltText }) => {
	var URLactual = window.location;
	const infoUsuario = JSON.parse(localStorage.getItem(USER_INFO));
	const infoEmail = infoUsuario !== null ? infoUsuario : '';
	const correo = infoEmail.email === null ? '' : infoEmail.email;
	const { t, i18n } = useTranslation();
	const classes = useStyles();
	const [anchorEl, setAnchorEl] = useState(null);
	const [languajeOption, setlanguajeOption] = useState(null);
	const loginUser = window.localStorage.getItem(USER_KEY);
	const [languaje, setLanguaje] = useState('en');
	const [profileImage, setProfileImage] = useState();
	const [anuncios, setAnuncios] = useState([]);
	const [terminoBusqueda, setTerminoBusqueda] = useState('');
	const [verResultado, setVerResultado] = useState(false);

	const handleSettingdToggle = (event) => setAnchorEl(event.currentTarget);
	const handleSelectionLanguaje = (event) =>
		setlanguajeOption(event.currentTarget);

	const handleCloseMenu = () => setAnchorEl(null);

	const handleCloseMenuLanguaje = () => setlanguajeOption(null);

	const onchangeLanguaje = () => {
		i18n.changeLanguage(languaje);
		languaje !== 'es' && setLanguaje('es');
	};
	const onchangeLanguajeen = () => {
		i18n.changeLanguage(languaje);
		languaje !== 'en' && setLanguaje('en');
	};
	const onchangeLanguajeFr = () => {
		i18n.changeLanguage(languaje);
		languaje !== 'fr' && setLanguaje('fr');
	};

	function handlerExit() {
		window.localStorage.removeItem(USER_KEY);
		window.localStorage.removeItem(USER_INFO);
	}

	useEffect(() => {
		const getUsuario = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_CONSULTAR_USUARIO}/${correo}`
				);
				const infoUsuario = Object.assign(response.data[0]);
				setProfileImage(infoUsuario.url_Foto_usuario);
			} catch (error) {
				console.log(error);
			}
		};
		const getAnuncio = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_BUSCAR_ANUNCIO}/${terminoBusqueda}`
				);
				const infoUsuario = Object.assign(response);
				setAnuncios(infoUsuario);
			} catch (error) {
				console.log(error);
			}
		};
		getAnuncio();
		getUsuario();
	}, [correo, terminoBusqueda]);

	const handlerBuscarAnuncio = (e) => {
		setTerminoBusqueda(e.target.value);
		setVerResultado(true);
	};

	const handleClickAway = () => {
		setVerResultado(false);
	};
	return (
		<>
			<AppBar className={`m-0 ${classes.appBar}`}>
				<Toolbar className={classes.toolBar}>
					<div className={`${classes.branding}`}>
						<a href="/">
							<img
								src={Logo}
								alt={logoAltText}
								className={`${classes.logo} pl-5 md:pl-10 w-56`}
							/>
						</a>
					</div>
					<Hidden xsDown>
						<ClickAwayListener onClickAway={handleClickAway}>
							<div className={`${classes.searchWrapper} m-0`}>
								<form className={classes.searchForm}>
									{URLactual.pathname !== '/perfil' && (
										<>
											{/* <IconButton
												aria-label="Search"
												className={classes.searchIcon}
											>
												<SearchIcon />
											</IconButton> */}

											<input
												className={`text-black ${classes.searchInput}`}
												type="text"
												placeholder={t('headerSearch')}
												onChange={(e) => handlerBuscarAnuncio(e)}
												autoFocus={true}
											/>
										</>
									)}
								</form>
							</div>
						</ClickAwayListener>
					</Hidden>
					<Hidden smUp>
						<span className="flexSpacer" />
					</Hidden>
					<>
						<div className="block ">
							{/* {Este menu de traduccion} */}
							<IconButton
								aria-label="User Settings"
								aria-owns={languajeOption ? 'user-menu' : null}
								aria-haspopup="true"
								color="inherit"
								onClick={handleSelectionLanguaje}
							>
								<GTranslateIcon />
							</IconButton>
							<Menu
								className=""
								id="languaje-menu"
								anchorEl={languajeOption}
								open={Boolean(languajeOption)}
								onClose={handleCloseMenuLanguaje}
							>
								<MenuItem onClick={handleCloseMenuLanguaje}>
									<ListItemText primary="Spanish" onClick={onchangeLanguaje} />
								</MenuItem>

								<MenuItem onClick={handleCloseMenuLanguaje}>
									<ListItemText
										primary="English"
										onClick={onchangeLanguajeen}
									/>
								</MenuItem>
								<MenuItem onClick={handleCloseMenuLanguaje}>
									<ListItemText
										primary="Français"
										onClick={onchangeLanguajeFr}
									/>
								</MenuItem>
							</Menu>
						</div>
					</>
					{!loginUser && (
						<IconButton className="mr-10 mt-1" href="/login">
							<p className="text-base font-semibold text-white hover:text-white">
								Ingresar
							</p>
						</IconButton>
					)}

					{loginUser && (
						<>
							<div className="block ">
								{/* {Este menu se vera solo cuando se haga login} */}
								<IconButton
									aria-label="User Settings"
									aria-owns={anchorEl ? 'user-menu' : null}
									aria-haspopup="true"
									color="inherit"
									onClick={handleSettingdToggle}
								>
									<Avatar aria-label="recipe" src={profileImage}></Avatar>
								</IconButton>

								<Menu
									className=""
									id="user-menu"
									anchorEl={anchorEl}
									open={Boolean(anchorEl)}
									onClose={handleCloseMenu}
								>
									<MenuItem onClick={handleCloseMenu}>
										<a
											href="/perfil"
											className="text-black flex justify-between"
											style={{ textDecoration: 'none' }}
										>
											<ListItemIcon>
												<AccountBoxIcon />
											</ListItemIcon>{' '}
											<ListItemText primary={t('headerProfile')} />
										</a>
									</MenuItem>

									<MenuItem onClick={handleCloseMenu}>
										<ListItemIcon>
											<ExitToAppIcon />
										</ListItemIcon>
										<a href="/">
											<ListItemText
												primary={t('headerExit')}
												onClick={handlerExit}
											/>
										</a>
									</MenuItem>
								</Menu>
							</div>
						</>
					)}
				</Toolbar>
				{verResultado && (
					<div className="container">
						{anuncios.data !== undefined && anuncios.data.length !== 0 ? (
							<div className="absolute left-10 md:left-50 w-10/12 bg-white shadow-md z-50 grid xsm:grid-cols-2 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-3 mx-2 mr-3">
								{anuncios.data.map((item) => (
									<a
										href={`/detalleAnuncio/${item.idAnuncio}`}
										target="_blanck"
										key={item.idAnuncio}
									>
										<div className="grid grid-cols-2 border-solid border-2 border-gray-300 w-full my-2 mr-2">
											<img
												src={item.urlFoto}
												alt="CES"
												style={{
													width: '100px',
													height: '80px',
													objectFit: 'contain',
												}}
											/>
											<CardContent className="p-0 m-0 mr-2">
												<p className="m-0 text-black text-lg font-bold">
													{item.titulo}
												</p>
												<p className="m-0 text-black text-sm font-semibold">
													{formatNumber(item.precio)}
												</p>
												<p
													variant="body2"
													color="textSecondary"
													component="p"
													className="m-0 text-black text-sm font-sans hidden md:block md:truncate"
												>
													{item.descripcion}
												</p>
											</CardContent>
										</div>
									</a>
								))}
							</div>
						) : (
							<div className="absolute left-10 md:left-50 w-10/12 bg-white shadow-md z-50 grid-cols-1 mx-2 mr-3">
								<div className="grid grid-cols-1 gap-4 justify-items-center w-full p-10">
									<center>
										<CircularProgress disableShrink />
									</center>
								</div>
							</div>
						)}
					</div>
				)}
			</AppBar>
		</>
	);
};

Header.prototypes = {
	logo: PropTypes.string,
	logoAltText: PropTypes.string,
};

export default Header;
