import React from 'react';
import { Redirect } from 'react-router-dom';
import { USER_KEY } from '../../types';
import { USER_INFO } from '../../types';

export default function Logout() {
	window.localStorage.removeItem(USER_KEY);
	window.localStorage.removeItem(USER_INFO);
	return <Redirect to="/" />;
}
