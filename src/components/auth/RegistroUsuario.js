import React, { useState } from 'react';
import { get, put, post } from '../../libs/AsyncHttpRequest';
import Swal from 'sweetalert2';
import { useForm, Controller } from 'react-hook-form';
import MenuItem from '@material-ui/core/MenuItem';
import * as lang from '../../Language/Countries';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';

const RegistroUsuario = () => {
	const { handleSubmit, register, errors, control } = useForm();
	const [cerrarErroresCorreo, setcerrarErroresCorreo] = useState(false);
	//const onSubmit = (values) => console.log(values);

	const onSubmit = async (values) => {
		console.log(values);
		try {
			const { data } = await post(
				`${process.env.REACT_APP_API_REGISTRAR_USUARIO}`,
				{
					dni: values.dni,
					nombre: values.nombre,
					apellido: values.apellido,
					email: values.email,
					nombreUsuario: values.usuario,
					contrasena: values.contrasena,
					telefono: values.telefono,
					direccion: values.direccion,
					municipio: values.ciudad,
					departamento: values.departamento,
					pais: values.pais,
				}
			);
			if (data) {
				Swal.fire('Bien hecho!', 'Su registro fue exitoso', 'success');
				setTimeout(function () {
					document.location.href = '/login';
				}, 3000);
			} else {
				Swal.fire('Error!', 'Algo salio mal intenta nuevamente', 'error');
			}
		} catch (error) {
			Swal.fire(
				'Error!',
				'La contraseña debe tener almenos entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula.',
				'error'
			);
		}
	};
	const [checked, setChecked] = useState(false);
	const [botonDisable, setBotonDisable] = useState(false);

	const handleChange = (event) => {
		setChecked(event.target.checked);
		setBotonDisable(!botonDisable);
	};
	return (
		<div className="container">
			<div className="m-2 grid-cols-1">
				{/** titulo formulario*/}
				<div className="mt-20 sm:mt-20 md:mt-20 lg:mt-10 mb-5 card-header">
					<p className="text-black text-2xl text-center font-bold">
						Registro de usuario
					</p>
				</div>

				<form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-1">
					<div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3">
						{/** Cedula */}
						<TextField
							className="w-full py-2 px-2 my-2"
							type="number"
							id="identifier"
							name="dni"
							variant="outlined"
							inputRef={register({
								required: 'El número de cédula es requerido',
							})}
							error={errors.name ? true : false}
							label={errors.name ? 'error' : 'Numero de cédula *'}
						/>
						{/** Nombre */}
						<TextField
							className="w-full py-2 px-2 my-2"
							type="text"
							id="nombre"
							name="nombre"
							variant="outlined"
							inputRef={register({
								required: 'El nombre es requerido',
							})}
							error={errors.name ? true : false}
							label={errors.name ? 'error' : 'Nombre *'}
						/>
						{/** Apellido */}
						<TextField
							className="w-full py-2 px-2 my-2"
							type="text"
							id="apellido"
							name="apellido"
							variant="outlined"
							inputRef={register({
								required: 'El apellido es requerido',
							})}
							error={errors.name ? true : false}
							label={errors.name ? 'error' : 'Apellido *'}
							helperText={errors.name ? errors.name.message : ''}
						/>
						{/** Telefono */}
						<TextField
							className="w-full py-2 px-2 my-2"
							type="text"
							id="telefono"
							name="telefono"
							variant="outlined"
							inputRef={register({
								required: 'El teléfono es requerido',
							})}
							error={errors.name ? true : false}
							label={errors.name ? 'error' : 'Teléfono *'}
							helperText={errors.name ? errors.name.message : ''}
						/>
						{/** Correo */}
						<TextField
							className="w-full py-2 px-2 my-2"
							type="text"
							id="email"
							name="email"
							variant="outlined"
							inputRef={register({
								required: 'El correo es requerido',
							})}
							error={errors.name ? true : false}
							label={errors.name ? 'error' : 'Correo electrónico *'}
							helperText={errors.name ? errors.name.message : ''}
						/>
						{/** Usuario */}
						<TextField
							className="w-full py-2 px-2 my-2"
							type="text"
							id="usuario"
							name="usuario"
							variant="outlined"
							inputRef={register({
								required: 'El usuario es requerido',
							})}
							error={errors.name ? true : false}
							label={errors.name ? 'error' : 'Usuario *'}
							helperText={errors.name ? errors.name.message : ''}
						/>
						{/** Contraseña */}
						<TextField
							className="w-full py-2 px-2 my-2"
							type="password"
							id="contrasena"
							name="contrasena"
							variant="outlined"
							inputRef={register({
								required: 'La contraseña es requerido',
							})}
							error={errors.name ? true : false}
							label={errors.name ? 'error' : 'Contraseña *'}
							helperText={errors.name ? errors.name.message : ''}
						/>
						{/** Direccion */}
						<TextField
							className="w-full py-2 px-2 my-2"
							type="text"
							id="direccion"
							name="direccion"
							variant="outlined"
							inputRef={register({
								required: 'El dirección es requerido',
							})}
							error={errors.name ? true : false}
							label={errors.name ? 'error' : 'Dirección *'}
							helperText={errors.name ? errors.name.message : ''}
						/>
						{/** Pais */}
						<FormControl
							variant="filled"
							className={` w-full pt-2 pb-0 mb-0 px-2 my-2`}
						>
							<InputLabel id="demo-simple-select-filled-label">
								País *
							</InputLabel>
							<Controller
								name="pais"
								control={control}
								defaultValue={'Colombia'}
								as={
									<Select
										name="pais"
										className="mb-0"
										labelId="demo-simple-select-filled-label"
										id="demo-simple-select-filled"
										label="Ciudad *"
										required
									>
										{lang.countries.map((countryName) => (
											<MenuItem key={countryName[1]} value={countryName[0]}>
												{countryName[0]}
											</MenuItem>
										))}
									</Select>
								}
							></Controller>
						</FormControl>
						{/** Departamento */}
						<TextField
							className="w-full py-2 px-2 my-2"
							type="text"
							id="departamento"
							name="departamento"
							variant="outlined"
							inputRef={register({
								required: 'El departamento es requerido',
							})}
							error={errors.name ? true : false}
							label={errors.name ? 'error' : 'Departamento *'}
							helperText={errors.name ? errors.name.message : ''}
						/>
						<TextField
							className="w-full py-2 px-2 my-2"
							type="text"
							id="ciudad"
							name="ciudad"
							variant="outlined"
							inputRef={register({
								required: 'El ciudad es requerido',
							})}
							error={errors.name ? true : false}
							label={errors.name ? 'error' : 'Ciudad *'}
							helperText={errors.name ? errors.name.message : ''}
						/>
					</div>
					<div className="flex flex-col sm:flex-row justify-between">
						<div className="flex">
							{' '}
							<Checkbox
								color="primary"
								checked={checked}
								onChange={handleChange}
								inputProps={{ 'aria-label': 'primary checkbox' }}
							/>
							<p className="pt-2">
								Acepto política de privacidad y tratamiento de datos{' '}
							</p>
						</div>
						<div className="grid grid-cols-2 sm:grid-cols-1 md:grid-cols-2">
							{botonDisable ? (
								<button
									type="submit"
									className="btn login_btn my-1 mx-2"
									variant="contained"
								>
									Registrar
								</button>
							) : (
								<div className=" login_btn my-1 mx-2 bg-blue-500 py-2 px-4 rounded opacity-50 cursor-not-allowed">
									Registrar
								</div>
							)}

							<a
								href="/login"
								className="btn p-0 my-1 mx-2 mt-0 login_btn  mr-0"
							>
								<p className="py-2 m-0 pb-0">Atrás</p>
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	);
};

export default RegistroUsuario;
