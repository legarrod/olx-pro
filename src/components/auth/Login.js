import React, { useState, useEffect, useCallback } from 'react';
import './style.auth.css';
import { useForm } from 'react-hook-form';
import { get, put, post } from '../../libs/AsyncHttpRequest';
import { USER_KEY } from '../../types';
import { USER_INFO } from '../../types';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';

const Login = () => {
	//estado para inicia sesion
	// const [usuario, guardarUsuario] = useState({
	// 	correo: '',
	// 	contrasena: '',
	// });
	//extraer el usuario
	// const { correo, contrasena } = usuario;
	const { handleSubmit, register, errors } = useForm();
	const [correo, setCorreo] = useState('');
	const [contrasena, setContrasena] = useState('');
	const [isLogin, setIsLogin] = useState(false);
	const [infousuario, setInfousuario] = useState({});
	const [usuarioNoExiste, setUsuarioNoExiste] = useState('');
	const [ocultaValidacion, setocultaValidacion] = useState(false);
	//const onSubmit = (values) => console.log(values);
	const onChange = (e) => {};
	const onSubmit = (value) => {
		//e.preventDefaul();
		setContrasena(value.contrasena);
		setCorreo(value.usuario);
	};

	// const handleRecordar = (e) => {
	// 	console.log(e.target.value);
	// };

	useEffect(() => {
		setIsLogin(false);
		const getUsuario = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_CONSULTAR_USUARIO}/${correo}`
				);
				const infoUsuario = response.data;
				const info = infoUsuario[0];

				if (info.email === correo && info.contrasena !== contrasena) {
					setocultaValidacion(true);
					setUsuarioNoExiste('El usuario o la contraseña es incorrecta');
				}
				if (info.email === correo && info.contrasena === contrasena) {
					window.localStorage.setItem(USER_KEY, contrasena);
					window.localStorage.setItem(USER_INFO, JSON.stringify(info));
					setIsLogin(true);
					document.location.href = '/perfil';
				}
			} catch (error) {
				if (error.message === "Cannot read property 'email' of undefined") {
					setocultaValidacion(true);
					setUsuarioNoExiste('El usuario no existe');
				}
			}
		};

		getUsuario();
	}, [correo, contrasena]);

	return (
		<div className="bg-black fondo-login">
			<div className="container  p-0 bg-cover">
				<div className="d-flex justify-content-center h-100 pt-20 ">
					<div className="card m-2">
						<div className="card-header justify-center">
							<h3>Iniciar seción</h3>
						</div>
						<div className="card-body">
							<form onSubmit={handleSubmit(onSubmit)}>
								<div className="input-group form-group">
									<div className="input-group-prepend">
										<span className="input-group-text">
											<i className="fas fa-user"></i>
										</span>
									</div>
									<input
										type="email"
										id="usuario"
										name="usuario"
										className="form-control"
										onChange={onChange}
										placeholder="Correo"
										ref={register({
											pattern: /[A-Za-z]/,
										})}
									/>
								</div>
								<div className="input-group form-group">
									<div className="input-group-prepend">
										<span className="input-group-text">
											<i className="fas fa-key"></i>
										</span>
									</div>
									<input
										type="password"
										className="form-control"
										id="contrasena"
										name="contrasena"
										onChange={onChange}
										placeholder="Contraseña"
										ref={register({
											pattern: /[A-Za-z]/,
										})}
									/>
								</div>
								{ocultaValidacion && (
									<div
										className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
										role="alert"
									>
										<strong class="font-bold">{usuarioNoExiste}</strong>
									</div>
								)}

								<div className="flex justify-between">
									<div className="row align-items-center remember pr-2 ml-2">
										<a href="/registroUsuario">Registrar</a>
									</div>
									<div className="row align-items-center remember pr-2 ml-2">
										<a href="/">Regresar</a>
									</div>
									<div className="form-group">
										<input
											type="submit"
											value="Ingresar"
											className="btn float-right login_btn"
											onClick={onchange}
										/>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Login;
