import React, { useState } from 'react';
import { get, put, post } from '../../libs/AsyncHttpRequest';
import Swal from 'sweetalert2';
import { useForm } from 'react-hook-form';

const Registro = () => {
	const { handleSubmit, register, errors } = useForm();
	const [cerrarErroresCorreo, setcerrarErroresCorreo] = useState(false);
	// const onSubmit = (values) => console.log(values);

	const onSubmit = async (values) => {
		console.log(values);
		try {
			const { data } = await post(
				`${process.env.REACT_APP_API_REGISTRAR_ADMINISTRADOR}`,
				{
					nombre: values.nombre,
					apellido: values.apellido,
					email: values.email,
					userName: values.userName,
					contrasena: values.contrasena,
					telefono: values.telefono,
				}
			);
			if (data) {
				Swal.fire(
					'Bien hecho!',
					'Se ha registrado un nuevo administrador',
					'success'
				);
			} else {
				Swal.fire('Error!', 'Algo salio mal intenta nuevamente', 'error');
			}
		} catch (error) {
			Swal.fire(
				'Error!',
				'La contraseña debe tener almenos entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula.',
				'error'
			);
		}
	};

	return (
		<div className="bg-black fondo-login">
			<div className="container  p-0 ">
				<div className="d-flex justify-content-center h-64 pt-20 ">
					<div className="card m-2">
						<div className="card-header">
							<h3>Registrar administrador</h3>
						</div>
						<div className="card-body">
							<form onSubmit={handleSubmit(onSubmit)}>
								<div className="input-group form-group">
									<div className="input-group-prepend">
										<span className="input-group-text">
											<i className="fas fa-user"></i>
										</span>
									</div>
									<input
										type="text"
										className="form-control"
										placeholder="Nombre"
										name="nombre"
										ref={register({
											pattern: /[A-Za-z]/,
										})}
									/>
									{errors.nombre && errors.nombre.message}
								</div>
								<div className="input-group form-group">
									<div className="input-group-prepend">
										<span className="input-group-text">
											<i className="fas fa-user"></i>
										</span>
									</div>
									<input
										type="text"
										className="form-control"
										placeholder="Apellido"
										name="apellido"
										ref={register({
											pattern: /[A-Za-z]/,
										})}
									/>
									{errors.apellido && errors.apellido.message}
								</div>
								<div className="input-group form-group">
									{/** Input del correo listo y validado */}
									<div className="input-group-prepend">
										<span className="input-group-text">
											<i className="fas fa-user"></i>
										</span>
									</div>
									<input
										className="form-control"
										type="email"
										placeholder="Correo"
										name="email"
										required
										ref={register({
											pattern: {
												value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
												message: (
													<div
														className="bg-red-100 mt-2 mb-2 text-base border border-red-400 text-red-700 px-4 py-3 rounded relative"
														role="alert"
													>
														<strong className="font-bold">Debe ingresar</strong>
														<span className="block sm:inline">
															un correo electrónico valido
														</span>
														<span className="absolute top-0 bottom-0 right-0 px-4 py-3">
															<svg
																className="fill-current h-6 w-6 text-red-500"
																role="button"
																xmlns="http://www.w3.org/2000/svg"
																viewBox="0 0 20 20"
															>
																<title>Close</title>
																<path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
															</svg>
														</span>
													</div>
												),
											},
										})}
									/>
									{errors.email && errors.email.message}
								</div>

								<div className="input-group form-group">
									<div className="input-group-prepend">
										<span className="input-group-text">
											<i className="fas fa-user"></i>
										</span>
									</div>
									<input
										type="text"
										className="form-control"
										placeholder="Teléfono"
										name="telefono"
										ref={register({
											maxLength: 10,
											minLength: 7,
										})}
									/>
									{errors.telefono && errors.telefono.message}
								</div>
								<div className="input-group form-group">
									<div className="input-group-prepend">
										<span className="input-group-text">
											<i className="fas fa-user"></i>
										</span>
									</div>

									<input
										type="text"
										className="form-control"
										placeholder="Usuario"
										name="userName"
										ref={register({
											validate: (value) =>
												value !== 'admin' || 'Buena elección',
										})}
									/>
									{errors.username && errors.username.message}
								</div>
								<div className="input-group form-group">
									<div className="input-group-prepend">
										<span className="input-group-text">
											<i className="fas fa-key"></i>
										</span>
									</div>

									<input
										className="form-control"
										type="password"
										placeholder="Contraseña"
										name="contrasena"
										required
										ref={register({
											pattern: {
												value: /[A-Za-z]{3}/,
											},
										})}
									/>

									{errors.contrasena && errors.contrasena.message}
								</div>
								<div className="flex justify-between">
									<div className="row pl-2 align-items-center remember pr-2 mr-10">
										<a href="/login" className="text-white">
											<button
												type="submit"
												className="btn float-right login_btn"
											>
												Atrás
											</button>
										</a>
									</div>
								</div>
								<div className="form-group">
									<button type="submit" className="btn float-right login_btn">
										Registrar
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Registro;
