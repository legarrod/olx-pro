import React, { useState, useEffect } from 'react';

import TextField from '@material-ui/core/TextField';
import { useForm } from 'react-hook-form';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { get, post } from '../../libs/AsyncHttpRequest';
import Swal from 'sweetalert2';
import MenuItem from '@material-ui/core/MenuItem';
import { USER_INFO } from '../../types';

import Button from '@material-ui/core/Button';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import 'filepond/dist/filepond.min.css';
import './style.css';

const DatosAnuncio = () => {
	const [verInformacion, setverInformacion] = useState([]);
	const infoUsuario = JSON.parse(localStorage.getItem(USER_INFO));
	const telefonoUsuario = infoUsuario.telefono;
	const { handleSubmit, register, errors } = useForm();
	const [verCategoria, setVerCategoria] = useState([]);
	const [loading, setloading] = useState(false);
	const [image, setImage] = useState('');
	const [categoria, setCategoria] = useState('');
	const [subCategoria, setSubCategoria] = useState('');
	const [subCategorias, setSubCategorias] = useState([]);
	const [visivleSubcategories, setVisivleSubcategories] = useState(false);

	const uploadImage = async (e) => {
		const files = e.target.files;
		const data = new FormData();
		data.append('file', files[0]);
		data.append('upload_preset', 'geekyimages');
		setloading(true);

		const res = await fetch(
			'https://api.cloudinary.com/v1_1/dhqgxx0cr/image/upload',
			{
				method: 'POST',
				body: data,
			}
		);
		const file = await res.json();
		setImage(file.secure_url);
		setloading(false);
	};

	const onSubmit = async (values) => {
		try {
			const { data } = await post(
				`${process.env.REACT_APP_API_REGISTRAR_ANUNCIO}`,
				{
					titulo: values.titulo,
					descripcion: values.descripcion,
					contacto: telefonoUsuario,
					subcategoria: {
						idSubCategoria: subCategoria.idSubCategoria,
					},
					usuario: {
						dni: infoUsuario.dni,
					},
					precio: values.precio,
					urlFoto: image,
				}
			);
			if (data) {
				Swal.fire(
					'Bien hecho!',
					'Se ha registrado un nuevo anuncio',
					'success'
				);
			} else {
				Swal.fire('Error!', 'Algo salio mal intenta nuevamente', 'error');
			}
			setTimeout(function () {
				window.location.reload(false);
			}, 3000);
		} catch (error) {
			Swal.fire(
				'Error!',
				'Algo sali mal intenta de nuevo y verifica toda la información',
				'error'
			);
		}
	};

	useEffect(() => {
		const getCategoria = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_ONLY_CATEGORIA}`
				);

				const infoUsuario = Object.assign(response.data);

				setVerCategoria(infoUsuario);
			} catch (error) {
				console.log(error);
			}
		};
		const getSubCategoria = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_ONLY_SUB_CATEGORIA}`
				);

				const infoUsuario = Object.assign(response.data);

				setverInformacion(infoUsuario);
			} catch (error) {
				console.log(error);
			}
		};
		getCategoria();
		getSubCategoria();
	}, []);
	useEffect(() => {
		const subCate = verInformacion.filter(
			(option) => option.categoria.idCategoria === categoria.idCategoria
		);
		setSubCategorias(subCate);
	}, [categoria, verInformacion]);
	const handleCategoria = (event) => {
		setCategoria(event.target.value);
		setVisivleSubcategories(true);
	};
	const handleSubCategoria = (e) => {
		setSubCategoria(e.target.value);
	};

	return (
		<div className="container">
			<div className="m-2 sm:mt-12 grid-cols-1">
				<div className=" m-2 ">
					<div className="text-black text-2xl mt-20 md:mt-20 lg:md-10 text-center font-bold">
						<h3>Crear anuncio</h3>
					</div>
					<div className="card-body mb-10 md:mb-32">
						<form
							onSubmit={handleSubmit(onSubmit)}
							className="grid grid-cols-1"
						>
							<div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3">
								{/** Titulo */}
								<TextField
									className="w-full py-2 px-2 my-2"
									type="text"
									id="titulo"
									name="titulo"
									variant="outlined"
									inputRef={register({
										required: 'Para registrar un anuncio debe crear un titulo',
									})}
									error={errors.name ? true : false}
									label={
										errors.name
											? 'error'
											: 'Titulo anuncio maximo 20 caracteres *'
									}
								/>
								{/** Precio */}
								<TextField
									className="w-full py-2 px-2 my-2"
									type="text"
									id="precio"
									name="precio"
									variant="outlined"
									inputRef={register({
										required: 'El precio es requerido',
									})}
									error={errors.name ? true : false}
									label={errors.name ? 'error' : 'Precio *'}
								/>
								{/** Descripcio */}
								<TextField
									className="w-full py-2 px-2 my-2"
									type="text"
									id="descripcion"
									name="descripcion"
									variant="outlined"
									inputRef={register({
										required: 'El descripción es requerido',
									})}
									error={errors.name ? true : false}
									label={errors.name ? 'error' : 'Descripción *'}
									helperText={errors.name ? errors.name.message : ''}
								/>
								{/** Acaponemos las categorias */}
								<FormControl
									variant="filled"
									className={` w-full pt-2 pb-0 mb-0 px-2 my-2`}
								>
									<InputLabel id="demo-simple-select-filled-label">
										Categorias *
									</InputLabel>

									<Select
										name="categorias"
										className="mb-0"
										labelId="demo-simple-select-filled-label"
										id="demo-simple-select-filled"
										label="Categoria"
										value={categoria}
										onChange={handleCategoria}
										// required
									>
										{verCategoria.map((categories) => (
											<MenuItem key={categories.idCategoria} value={categories}>
												{categories.nombre}
											</MenuItem>
										))}
									</Select>
								</FormControl>

								{visivleSubcategories && (
									<FormControl
										variant="filled"
										className={` w-full pt-2 pb-0 mb-0 px-2 my-2`}
									>
										<InputLabel id="demo-simple-select-filled-label">
											Subcategorias
										</InputLabel>

										<Select
											name="subcategorias"
											className="mb-0"
											labelId="demo-simple-select-filled-label"
											id="demo-simple-select-filled"
											label="Subcategoria"
											value={subCategoria}
											onChange={handleSubCategoria}
											// required
										>
											{subCategorias.map((categories) => (
												<MenuItem
													key={categories.idSubCategoria}
													value={categories}
												>
													{categories.descripcion}
												</MenuItem>
											))}
										</Select>
									</FormControl>
								)}
							</div>

							<>
								<input type="file" name="file" onChange={uploadImage} />

								{loading ? (
									<p>Subiendo...</p>
								) : (
									<img
										className="mt-2"
										src={image}
										style={{ width: '200px', height: 'auto' }}
										alt=""
									/>
								)}
							</>

							<div>
								<Button
									type="submit"
									className="hover:text-orange-100 float-right"
									color="primary"
									variant="contained"
								>
									CREAR
								</Button>

								<Button
									variant="contained"
									className="hover:text-orange-100"
									color="primary"
									href="/perfil"
								>
									REGRESAR
								</Button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};

export default DatosAnuncio;
