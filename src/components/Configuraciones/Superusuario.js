import React, { useState, useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import CardContent from '@material-ui/core/CardContent';
import { Usuario } from '../../bd';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import ImageUploading from 'react-images-uploading';
import ActualizarAnuncio from '../Anuncios/ActualizarAnuncio';
import { Anunciobd } from '../../bd';
import { USER_INFO } from '../../types';
import { get, put, post } from '../../libs/AsyncHttpRequest';
import Swal from 'sweetalert2';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { useForm, Controller } from 'react-hook-form';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import * as lang from '../../Language/Countries';
import { useTranslation } from 'react-i18next';
import Listausuarios from './superUsuarioTools/Listausuarios';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import {
	Image,
	Video,
	Transformation,
	CloudinaryContext,
} from 'cloudinary-react';
import { Cloudinary } from 'cloudinary-core';
import Listaanuncios from './superUsuarioTools/Listaanuncios';
import ListarCategorias from './superUsuarioTools/ListarCategorias';

const PerfilUsuario = () => {
	const [images, setImages] = React.useState([
		{
			data_url: Usuario.foto,
		},
	]);
	const infoUsuario = JSON.parse(localStorage.getItem(USER_INFO));
	const correo = infoUsuario.email;
	const dni = infoUsuario.dni;
	const maxNumber = 1;
	const [verInformacion, setverInformacion] = useState({});
	const [anuncios, setAnuncios] = useState([]);
	const [allAnuncios, setAllAnuncios] = useState([]);
	const [loading, setloading] = useState(false);
	const [image, setImage] = useState('');
	const [fotoAvatar, setFotoAvatar] = useState('');
	const [value, setValue] = React.useState(0);

	const handleChange = (event, newValue) => {
		setValue(newValue);
	};

	const uploadImage = async (e) => {
		const files = e.target.files;
		const data = new FormData();
		data.append('file', files[0]);
		data.append('upload_preset', 'geekyimages');
		setloading(true);

		const res = await fetch(
			'https://api.cloudinary.com/v1_1/dhqgxx0cr/image/upload',
			{
				method: 'POST',
				body: data,
			}
		);
		const file = await res.json();
		setImage(file.secure_url);
		setloading(false);
	};
	const { t, i18n } = useTranslation();
	const [actualizarInformacio, setActualizarInformacio] = useState(false);
	const [usuarios, setUsuarios] = useState([]);
	const [categorias, setCategorias] = useState([]);
	const handlerSetInormation = () => {
		setActualizarInformacio(!actualizarInformacio);
	};
	const { handleSubmit, register, errors, control } = useForm();
	//const onSubmit = (values) => console.log(values);

	const onSubmit = async (values) => {
		try {
			const { data } = await put(
				`${process.env.REACT_APP_API_ACTUALIZAR_USUARIO}/${infoUsuario.dni}`,
				{
					dni: infoUsuario.dni,
					nombre: values.nombre,
					apellido: values.apellido,
					email: values.email,
					nombreUsuario: infoUsuario.nombreUsuario,
					contrasena: values.contrasena,
					telefono: values.telefono,
					direccion: values.direccion,
					municipio: values.municipio,
					pais: values.pais,
					anuncios: [],
					chats: [],
					departamento: values.departamento,
					url_foto_usuario: image,
				}
			);
			if (data) {
				Swal.fire(
					'Bien hecho!',
					'Su información se actualizó correctamente',
					'success'
				);
			} else {
				Swal.fire('Error!', 'Algo salio mal intenta nuevamente', 'error');
			}
		} catch (error) {
			Swal.fire('Algo salio mal', 'Intentalo de nuevo', 'error');
		}
	};

	useEffect(() => {
		const getUsuario = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_CONSULTAR_USUARIO}/${correo}`
				);

				const infoUsuario = Object.assign(response.data[0]);
				setFotoAvatar(infoUsuario.url_Foto_usuario);
				setverInformacion(infoUsuario);
			} catch (error) {
				console.log(error);
			}
		};
		const getAnuncios = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_ANUNCIO_USUARIO}`
				);
				const infoAnuncio = Object.assign(response.data);
				const subCate = infoAnuncio.filter(
					(option) => option.usuario && option.usuario.dni === infoUsuario.dni
				);
				setAllAnuncios(infoAnuncio);
				setAnuncios(subCate);
			} catch (error) {
				console.log(error);
			}
		};

		const getUsuarios = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_USUARIOS}`
				);
				const infoAnuncio = Object.assign(response.data);

				setUsuarios(infoAnuncio);
			} catch (error) {
				console.log(error);
			}
		};
		const getCategorias = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_ONLY_SUB_CATEGORIA}`
				);
				const infoAnuncio = Object.assign(response.data);

				setCategorias(infoAnuncio);
			} catch (error) {
				console.log(error);
			}
		};
		getCategorias();
		getAnuncios();
		getUsuario();
		getUsuarios();
	}, [correo, infoUsuario.dni]);

	function TabPanel(props) {
		const { children, value, index, ...other } = props;

		return (
			<div
				role="tabpanel"
				hidden={value !== index}
				id={`full-width-tabpanel-${index}`}
				aria-labelledby={`full-width-tab-${index}`}
				{...other}
			>
				{value === index && (
					<Box p={3}>
						<Typography>{children}</Typography>
					</Box>
				)}
			</div>
		);
	}
	function a11yProps(index) {
		return {
			id: `scrollable-auto-tab-${index}`,
			'aria-controls': `scrollable-auto-tabpanel-${index}`,
		};
	}

	return (
		<div className="md:grid-cols-2 md:mx-20 my-20">
			<Paper
				elevation={3}
				style={{ marginTop: '10px' }}
				className="p-0 md:p-12"
			>
				<CardContent>
					<Grid container className="flex justify-center" spacing={3}>
						<Grid item xs={12} sm={5} md={5} className="text-center">
							<div className="">
								<center>
									<Avatar
										alt={infoUsuario.nombre + ' ' + infoUsuario.apellido}
										// src={`${process.env.PUBLIC_URL}/static/images/avatar.jpg`}
										src={verInformacion.url_Foto_usuario}
										className="bg-black"
										style={{ width: 200, height: 200 }}
									/>
								</center>
							</div>
						</Grid>
						<Grid item xs={12} sm={7} md={7}>
							<Typography variant="h6" gutterBottom>
								<b>{infoUsuario.nombre + ' ' + infoUsuario.apellido}</b>
							</Typography>
							<Typography variant="caption" gutterBottom>
								{`@${infoUsuario.nombreUsuario}`}
							</Typography>
							<Typography variant="body1" gutterBottom>
								{`${t('profile.phone')} +57 ${infoUsuario.telefono} `}
							</Typography>
							<Typography variant="body1" gutterBottom>
								<Link to="/">{infoUsuario.correo}</Link>
							</Typography>
							<center>
								<div className="flex flex-wrap">
									<a href="#form-actualizar">
										<Button
											className="m-1"
											variant="contained"
											color="primary"
											onClick={handlerSetInormation}
										>
											{t('profile.editprofile')}
										</Button>
									</a>
								</div>
							</center>
						</Grid>
					</Grid>
					{actualizarInformacio && (
						<>
							<div className="">
								<div id="form-actualizar" className="mt-8">
									<p className="font-semibold text-lg">
										{t('profile.updateInformatio')}
									</p>
								</div>
								<div className="flex flex-wrap justify-center">
									<form onSubmit={handleSubmit(onSubmit)}>
										<div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3">
											<TextField
												className="w-full py-2 px-2 my-2"
												type="text"
												id="nombre"
												name="nombre"
												variant="outlined"
												defaultValue={verInformacion.nombre}
												style={{ margin: 8 }}
												inputRef={register()}
												error={errors.name ? true : false}
												label={errors.name ? 'error' : t('profile.inputName')}
											/>
											<TextField
												className="w-full py-2 px-2 my-2"
												type="text"
												id="apellido"
												name="apellido"
												variant="outlined"
												defaultValue={verInformacion.apellido}
												style={{ margin: 8 }}
												inputRef={register()}
												error={errors.name ? true : false}
												label={
													errors.name ? 'error' : t('profile.inputlastName')
												}
											/>
											<TextField
												className="w-full py-2 px-2 my-2"
												type="text"
												id="telefono"
												name="telefono"
												variant="outlined"
												defaultValue={verInformacion.telefono}
												style={{ margin: 8 }}
												inputRef={register()}
												error={errors.name ? true : false}
												label={errors.name ? 'error' : t('profile.phone')}
											/>
											<TextField
												className="w-full py-2 px-2 my-2"
												type="email"
												id="email"
												name="email"
												variant="outlined"
												defaultValue={verInformacion.email}
												style={{ margin: 8 }}
												inputRef={register()}
												error={errors.name ? true : false}
												label={errors.name ? 'error' : t('profile.inputEmail')}
											/>
											<TextField
												className="w-full py-2 px-2 my-2"
												type="text"
												id="usuario"
												name="usuario"
												variant="outlined"
												defaultValue={verInformacion.nombreUsuario}
												disabled
												style={{ margin: 8 }}
												inputRef={register()}
												error={errors.name ? true : false}
												label={
													errors.name ? 'error' : t('profile.inputUserName')
												}
											/>
											<TextField
												className="w-full py-2 px-2 my-2"
												type="password"
												id="contrasena"
												name="contrasena"
												variant="outlined"
												defaultValue={verInformacion.contrasena}
												style={{ margin: 8 }}
												inputRef={register()}
												error={errors.name ? true : false}
												label={
													errors.name ? 'error' : t('profile.inputPassword')
												}
											/>
											<TextField
												className="w-full py-2 px-2 my-2"
												type="text"
												id="direccion"
												name="direccion"
												variant="outlined"
												defaultValue={verInformacion.direccion}
												style={{ margin: 8 }}
												inputRef={register()}
												error={errors.name ? true : false}
												label={errors.name ? 'error' : t('profile.inputAdress')}
											/>
											<FormControl
												variant="filled"
												className={`pt-2 pb-0 mb-0 px-2 my-0 w-full`}
											>
												<InputLabel id="demo-simple-select-filled-label">
													{t('profile.inputCountry')}
												</InputLabel>
												<Controller
													name="pais"
													control={control}
													defaultValue="Colombia"
													as={
														<Select
															name="pais"
															className="mb-0"
															labelId="demo-simple-select-filled-label"
															id="demo-simple-select-filled"
														>
															{lang.countries.map((countryName) => (
																<MenuItem
																	key={countryName[1]}
																	value={countryName[0]}
																>
																	{countryName[0]}
																</MenuItem>
															))}
														</Select>
													}
												></Controller>
											</FormControl>
											<TextField
												className="w-full py-2 px-2 my-2"
												type="text"
												id="departamento"
												name="departamento"
												variant="outlined"
												defaultValue={verInformacion.departamento}
												style={{ margin: 8 }}
												inputRef={register()}
												error={errors.name ? true : false}
												label={errors.name ? 'error' : t('profile.inputState')}
											/>
											<TextField
												className="w-full py-2 px-2 my-2"
												type="text"
												id="municipio"
												name="municipio"
												variant="outlined"
												defaultValue={verInformacion.municipio}
												style={{ margin: 8 }}
												inputRef={register()}
												error={errors.name ? true : false}
												label={errors.name ? 'error' : t('profile.inputCity')}
											/>
											<div className="ml-12">
												<input type="file" name="file" onChange={uploadImage} />

												{loading ? (
													<p>Subiendo...</p>
												) : (
													<img
														src={image}
														style={{ width: '200px', height: 'auto' }}
													/>
												)}
											</div>
										</div>
										<Button
											type="submit"
											className="m-1"
											variant="contained"
											color="primary"
										>
											{t('profile.updateInformation')}
										</Button>
										{/* <button
											type="submit"
											variant="contained"
											color="primary"
											className="btn float-right login_btn"
										>
											Actualziar información
										</button> */}
									</form>
								</div>
							</div>
							<div></div>
						</>
					)}
				</CardContent>
			</Paper>

			<Paper
				elevation={3}
				style={{ marginTop: '10px' }}
				className="p-0 md:p-12 mt-12"
			>
				<Tabs
					value={value}
					onChange={handleChange}
					indicatorColor="primary"
					textColor="primary"
					centered
				>
					<Tab label="Usuarios" {...a11yProps(0)} />
					<Tab label="Publicaciones" {...a11yProps(1)} />
					<Tab label="Categorias" {...a11yProps(2)} />
				</Tabs>
				<TabPanel value={value} index={0}>
					<Listausuarios usuarios={usuarios} />
				</TabPanel>

				<TabPanel value={value} index={1}>
					<Listaanuncios allAnuncios={allAnuncios} />
				</TabPanel>
				<TabPanel value={value} index={2}>
					<ListarCategorias categorias={categorias} />
				</TabPanel>
			</Paper>
		</div>
	);
};

export default PerfilUsuario;
