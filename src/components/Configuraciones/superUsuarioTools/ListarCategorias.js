import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import { get } from '../../../libs/AsyncHttpRequest';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { AiFillEdit } from 'react-icons/ai';
import { AiFillDelete } from 'react-icons/ai';
import Swal from 'sweetalert2';

const useRowStyles = makeStyles({
	root: {
		'& > *': {
			borderBottom: 'unset',
		},
	},
	body: {
		width: '200px',
		height: '30px',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
	},
});

function Row(props) {
	const { row } = props;
	const [open, setOpen] = React.useState(false);
	const classes = useRowStyles();

	const viewImage = (url) => {
		Swal.fire({
			imageUrl: url,
			imageHeight: 400,
			imageAlt: 'CES',
		});
	};

	return (
		<React.Fragment>
			<TableRow className={classes.root}>
				<TableCell component="th" scope="row">
					{row.categoria.nombre}
				</TableCell>
				<TableCell align="left">{row.descripcion}</TableCell>
				<TableCell>
					<div className="flex justify-center">
						<AiFillEdit className="text-2xl" />
						<AiFillDelete className="text-2xl" />
					</div>
				</TableCell>
			</TableRow>
		</React.Fragment>
	);
}

const ListarCategorias = ({ categorias }) => {
	console.log(categorias);
	return (
		<TableContainer component={Paper}>
			<Table aria-label="collapsible table">
				<TableHead>
					<TableRow>
						<TableCell>Categoria</TableCell>
						<TableCell align="center">Sub-Categoria</TableCell>

						<TableCell>Acciones</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{categorias.map((row) => (
						<Row key={row.name} row={row} />
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
};

export default ListarCategorias;
