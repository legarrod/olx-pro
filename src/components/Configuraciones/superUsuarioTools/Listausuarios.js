import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import { get } from '../../../libs/AsyncHttpRequest';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { AiFillEdit } from 'react-icons/ai';
import { AiFillDelete } from 'react-icons/ai';

const useRowStyles = makeStyles({
	root: {
		'& > *': {
			borderBottom: 'unset',
		},
	},
});

function Row(props) {
	const { row } = props;
	const [open, setOpen] = React.useState(false);
	const classes = useRowStyles();

	return (
		<React.Fragment>
			<TableRow className={classes.root}>
				<TableCell component="th" scope="row">
					{row.nombre}
				</TableCell>
				<TableCell align="left">{row.apellido}</TableCell>
				<TableCell align="left">{row.email}</TableCell>
				<TableCell align="left">{row.telefono}</TableCell>
				<TableCell align="center">{row.dni}</TableCell>
				<TableCell align="left">{row.nombreUsuario}</TableCell>
				<TableCell align="left">{'******'}</TableCell>
				<TableCell align="left">{row.departamento}</TableCell>
				<TableCell align="left">{row.municipio}</TableCell>
				<TableCell align="left">{row.direccion}</TableCell>
				<TableCell>
					<div className="flex justify-center">
						<AiFillEdit className="text-2xl" />
						<AiFillDelete className="text-2xl" />
					</div>
				</TableCell>
			</TableRow>
		</React.Fragment>
	);
}

const Listausuarios = ({ usuarios }) => {
	return (
		<TableContainer component={Paper}>
			<Table aria-label="collapsible table">
				<TableHead>
					<TableRow>
						<TableCell>Nombre</TableCell>
						<TableCell align="center">Apellido</TableCell>
						<TableCell align="center">Correo</TableCell>
						<TableCell align="center">Teléfono</TableCell>
						<TableCell align="center">Cédula</TableCell>
						<TableCell align="center">Usuario</TableCell>
						<TableCell align="center">Contraseña</TableCell>
						<TableCell align="center">Departamento</TableCell>
						<TableCell align="center">Ciudad</TableCell>
						<TableCell>Dirección</TableCell>
						<TableCell>Acciones</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{usuarios.map((row) => (
						<Row key={row.name} row={row} />
					))}
				</TableBody>
			</Table>
		</TableContainer>
	);
};

export default Listausuarios;
