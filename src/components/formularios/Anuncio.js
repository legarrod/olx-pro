import React from 'react';

const Anuncio = () => {
	return (
		<div>
			<div className="container my-5">
				<div className="row justify-content-center h-100">
					<div className="col-md-12 my-5">
						<div className="card my-auto">
							<div className="card-header">
								<h4 className="text-uppercase text-center">
									datos del anuncio
								</h4>
							</div>
							<div className="card-body">
								<form>
									<div className="form-row">
										<div className="form-group col-lg-6 col-md-12 col-sm-12 col-12 icon">
											<label htmlFor="">Titulo</label>

											<span className="fa fa-eye form-control-feedback"></span>
											<input
												type="text"
												className="form-control"
												placeholder="Ingrese un titulo para su anuncio"
												required
											/>
										</div>
										<div className="form-group col-lg-6 col-md-12 col-sm-12 col-12 icon">
											<label htmlFor="">Precio</label>

											<span className="fa fa-usd form-control-feedback"></span>
											<input
												type="number"
												className="form-control"
												placeholder="Ingrese el precio de su anuncio"
												required
											/>
										</div>
									</div>

									<label htmlFor="">Descripción</label>

									<textarea
										className="form-control mb-3"
										cols="5"
										rows="3"
										placeholder="Escriba una descripción de su anuncio"
										required
									></textarea>

									<div className="form-row">
										<div className="container">
											<div className="wrapper">
												<div className="image">
													<img src="" alt="" />
												</div>
												<div className="content">
													<div className="icon">
														<i className="fa fa-file-image-o"></i>
													</div>
												</div>
												<div id="cancel-btn">
													<i className="fa fa-times"></i>
												</div>
											</div>
											<button
												className="btn-primary text-uppercase font-weight-bold"
												id="custom-btn"
											>
												foto
											</button>
											<input id="default-btn" type="file" hidden />
										</div>
									</div>

									<button
										type="submit"
										className="btn btn-primary text-uppercase font-weight-bold float-right text-center"
										style={{ width: '200px' }}
									>
										Publicar
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Anuncio;
