import React, { useEffect, useState } from 'react';
import { BsChevronCompactDown } from 'react-icons/bs';
import { get } from '../../libs/AsyncHttpRequest';
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

const MenuCategorias = ({ setIdSubcategoria }) => {
	const [visivleSubcategories, setVisivleSubcategories] = useState(false);
	const [verCategoria, setVerCategoria] = useState([]);
	const [subCategorias, setSubCategorias] = useState([]);
	const [categoriaPrincipal, setCategoriaPrincipal] = useState([]);
	const [verInformacion, setverInformacion] = useState([]);

	useEffect(() => {
		const getCategoria = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_ONLY_CATEGORIA}`
				);

				const infoUsuario = Object.assign(response.data);

				setVerCategoria(infoUsuario);
				setCategoriaPrincipal(
					infoUsuario.filter((item) => item.idCategoria < 13)
				);
			} catch (error) {
				console.log(error);
			}
		};
		const getSubCategoria = async () => {
			try {
				const response = await get(
					`${process.env.REACT_APP_API_LISTAR_ONLY_SUB_CATEGORIA}`
				);

				const infoUsuario = Object.assign(response.data);

				setverInformacion(infoUsuario);
			} catch (error) {
				console.log(error);
			}
		};
		getCategoria();
		getSubCategoria();
	}, []);

	const handleCategorias = () => {
		setVisivleSubcategories((prev) => !prev);
	};
	const handleClickAway = () => {
		setVisivleSubcategories(false);
	};

	const handleSubcategorias = (value) => {
		setIdSubcategoria(value);
	};
	return (
		<>
			<div className="container flex mt-16 sm:mt-16 md:mt-16 lg:mt-2">
				<ClickAwayListener onClickAway={handleClickAway}>
					<div className="flex items-center w-32" onClick={handleCategorias}>
						<Button
							className="mt-1 P-3 text-xl  font-bold  mr-2"
							startIcon={<BsChevronCompactDown className="text-2xl mt-1" />}
						>
							CATEGORIAS{' '}
						</Button>
					</div>
				</ClickAwayListener>
				<div className="container  hidden lg:block">
					<div className="flex justify-between bg-white pt-2 z-50">
						{categoriaPrincipal.map((item) => (
							<a href="/">
								<p key={item.idCategoria} className="font-semibold ml-2">
									{item.nombre}
								</p>
							</a>
						))}
					</div>
				</div>
			</div>
			{visivleSubcategories && (
				<div className="container">
					<div className="absolute left-10 md:left-50 w-64 bg-white shadow-md z-50">
						{verCategoria.map((item) => (
							<a href="/">
								<p
									key={item.idCategoria}
									className="left-50  font-semibold ml-2"
									onChange={handleSubcategorias(item.idCategoria)}
								>
									{item.nombre}
								</p>
							</a>
						))}
					</div>
				</div>
			)}
		</>
	);
};

export default MenuCategorias;
