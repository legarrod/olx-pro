import React from 'react';
import Banner from '../../assets/img/baner.jpg';

import Carousel from 'nuka-carousel';

const Navbar = () => ({
	mixins: [Carousel.ControllerMixin],
	render() {
		return (
			<div className="hidden md:block">
				<img className="w-full h-40" src={Banner} alt="CES" />
			</div>
		);
	},
});

export default Navbar;
