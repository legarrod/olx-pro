import React, { useState } from 'react';
//import Categorias from '../Categorias/Categorias';
import Anuncios from '../Anuncios/Anuncios';
import { Anunciobd } from '../../bd';
import Navbar from '../Navbar/Navbar';
import AnuncioDestacado from '../Anuncios/AnuncioDestacado';
import Header from '../Header/Header';
import MenuCategorias from '../MenuCategorias/MenuCategorias';
import { useTranslation } from 'react-i18next';

const Home = () => {
	const [idSubcategoria, setIdSubcategoria] = useState();
	const { t, i18n } = useTranslation();
	const handlerSubcategoria = () => {};
	console.log(idSubcategoria);
	return (
		<div>
			<Header></Header>
			<MenuCategorias setIdSubcategoria={setIdSubcategoria} />
			<Navbar></Navbar>

			<div className="mx-6 pt-10 xsm:pt-20 md:pt-3">
				<p className="font-bold text-2xl mt-2 mb-0">{t('home.featured')}</p>
				<AnuncioDestacado></AnuncioDestacado>
				<p className="font-bold text-2xl mt-5 mb-0">{t('home.featured')}</p>
				<Anuncios anuncios={Anunciobd}></Anuncios>
			</div>
		</div>
	);
};

export default Home;
