export const Anunciobd = [
	{
		id: 1,
		titulo: 'Carro barato',
		precio: '35.000.000',
		descripcion:
			'Camioneta en excelentes condiciones, seguro 31 de octubre 2020, Tecno hasta 1 de noviembre del 2020, llantas nuevas, cojineria en perfecto estado, vidrios eléctricos funcionales, execelente estado de pintura, no podridos. Lista para traspaso. Al día! No tiene fugas de aceite, caja automática en perfectas condiciones.',
		foto:
			'https://apollo-virginia.akamaized.net/v1/files/ktmb46m27fej1-CO/image;s=1080x1080',
	},
	{
		id: 2,
		titulo: 'Hermosa camioneta',
		precio: '25.000.000',
		descripcion: 'PICANTO 2014 AUTOMÁTICO NEGOCIABLE',
		foto:
			'https://apollo-virginia.akamaized.net/v1/files/3qe4p44o914r2-CO/image;s=1080x1080',
	},
	{
		id: 3,
		titulo: 'Carro a muy buen precio',
		precio: '30.000.000',
		descripcion:
			'Kia Picanto ion Mecanico 2015, al día para traspaso excelente estado',
		foto:
			'https://apollo-virginia.akamaized.net/v1/files/cwk3az05dllv3-CO/image;s=1080x1080',
	},
	{
		id: 4,
		titulo: 'Vendo muy barato',
		precio: '32.000.000',
		descripcion: 'Adquiere el carro de tus sueños',
		foto:
			'https://apollo-virginia.akamaized.net/v1/files/7nrp6750xyif-CO/image;s=1080x1080',
	},
	{
		id: 5,
		titulo: 'Adorno para mesa',
		precio: '24.900',
		descripcion:
			'LINDA MATERA BULBASAUR LOW POLY O POLIGONAL PIXELADO. RECORDANDO LOS PROMEROS DIAS DE POKEMON. PARA COLECCION, DECORACION,ESCRITORIO Y JARDIN. TENEMOS MAS POKEMONES LOW POLI PARA TI.',
		foto:
			'https://apollo-virginia.akamaized.net/v1/files/0tg4qr1gkcm-CO/image;s=1080x1080',
	},
	{
		id: 6,
		titulo: 'Lavadora',
		precio: '479.000',
		descripcion: 'Adquiere el carro de tus sueños',
		foto:
			'https://apollo-virginia.akamaized.net/v1/files/ymgsi87knrmc1-CO/image;s=1080x1080',
	},
	{
		id: 7,
		titulo: 'Diadema gamer',
		precio: '110.000',
		descripcion: 'DIADEMA GAMER CON LUZ LED RGB',
		foto:
			'https://apollo-virginia.akamaized.net/v1/files/kdhat9g3wrek1-CO/image;s=1080x1080',
	},
];

export const Categoria = [
	{
		id: 10,
		nombre: 'Joyeria',
		icono: 'https://s3.amazonaws.com/recursos.proyecto/categorias1.png',
	},
	{
		id: 11,
		nombre: 'Niños',
		icono: 'https://s3.amazonaws.com/recursos.proyecto/categorias2.png',
	},
	{
		id: 12,
		nombre: 'Autos',
		icono: 'https://s3.amazonaws.com/recursos.proyecto/categorias3.png',
	},
	{
		id: 13,
		nombre: 'Ropa dama',
		icono: 'https://s3.amazonaws.com/recursos.proyecto/categorias5.png',
	},
	{
		id: 14,
		nombre: 'Ropa Caballero',
		icono: 'https://s3.amazonaws.com/recursos.proyecto/categorias4.png',
	},
];

export const Usuario = {
	id: '1',
	nombre: 'James',
	apellido: 'Angel',
	correo: 'chica@gmail.com',
	telefono: '3156985623',
	usuario: 'elMan',
	pasword: 'mepillaron2020',
	foto:
		'https://competenciasdelsiglo21.com/wp-content/uploads/2019/04/disc-perfil-c-azul-948x640.jpg',
};
