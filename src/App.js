import React from 'react';
import Routes from './components/Routes';
import Footer from './components/Footer/Footer';

import InstallPWA from './components/PwaListen/index';

function App() {
	return (
		<div className="mt-0">
			<InstallPWA></InstallPWA>

			<Routes />

			<Footer></Footer>
		</div>
	);
}

export default App;
