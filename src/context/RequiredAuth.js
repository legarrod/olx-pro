import React from 'react';
import { Redirect } from 'react-router-dom';
import { USER_KEY } from '../types';

export function RequiredAuth({ Component }) {
	if (!window.localStorage.getItem(USER_KEY)) {
		return <Redirect to="/login" />;
	}
	return <Component />;
}

export function RequiredAuthSuper({ Component }) {
	if (!window.localStorage.getItem(USER_KEY)) {
		return <Redirect to="/login" />;
	}
	return <Component />;
}
